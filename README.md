# Robbers

Robbers is a microservice that gathers information from crypto markets and then stores it in the database.

## Installation
Clone current repository and install required packages via `npm install`. After that, execute `npm run build` to compile TypeScript into JavaScript.

After build, configure environment variables and then execute `npm start`.

## Configuration
Make a copy of `.env.example` file and rename it to `.env` and then open it.

### Environment variables
- **DB_HOST** - network hostname (or IP address) of your database location.
- **DB_PORT** - network port.
- **DB_NAME** - database name.
- **DB_USER** - database user.
- **DB_PASSWORD** - database password.