import axios, { AxiosResponse } from 'axios';
import { throwError } from '../common/service';
import { getPairsForMarket, renamePairs } from '../common/process';
import { writeToDb } from '../common/db';
import { Pool } from 'pg';

async function prepareData(raw: BinanceResponse[]): Promise<PairObject[]> {
    return new Promise(async resolve => {
        const timeNow: string = new Date().toLocaleString();
        const pairs: string[] = await getPairsForMarket(Market.Binance);
        let data: PairObject[] = await new Promise(resolve => {
            let marketData: PairObject[] = [];
            raw.forEach(element => {
                if (pairs.includes(element.symbol)) {
                    marketData.push({ market: Market.Binance, pair: element.symbol, price: +element.price, date: timeNow });
                }
            });
            if (marketData.length === 0) {
                console.log(`Pairs: ${pairs}\nRaw response data: ${raw}`);
                throwError(`Pairs info not found for GET request to market '${Market.Binance}'`);
            }
            else {
                resolve(marketData);
            }
        }) as PairObject[];
        resolve(data);
    });
}

function callBinance(pool: Pool): Promise<void> {
    return new Promise(async resolve => {
        axios.get('http://api.binance.com/api/v3/ticker/price')
        .then(async function (response: AxiosResponse) {
            let data: PairObject[] = await prepareData(response.data as BinanceResponse[]) as PairObject[];
            data = await renamePairs(data);
            await writeToDb(pool, data);
            resolve();
        })
        .catch((error: Error) => {
            console.error(`Binance is unreachable or something else gone wrong. ${error}`);
            resolve();
        });
    });
}

export { callBinance };