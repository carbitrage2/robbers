import axios, { AxiosResponse } from 'axios';
import { throwError } from '../common/service';
import { getPairsForMarket, renamePairs } from '../common/process';
import { writeToDb } from '../common/db';
import { Pool } from 'pg';

async function prepareData(raw: PoloniexResponse): Promise<PairObject[]> {
    return new Promise(async resolve => {
        const timeNow: string = new Date().toLocaleString();
        const pairs: string[] = await getPairsForMarket(Market.Poloniex);
        let data: PairObject[] = await new Promise(resolve => {
            let marketData: PairObject[] = [];
            Object.keys(raw).forEach(key => {
                if (pairs.includes(key)) {
                    const value = raw[key]
                    marketData.push({ market: Market.Poloniex, pair: key, price: +value.last, date: timeNow })
                }
            });
            if (marketData.length === 0) {
                console.log(`Pairs: ${pairs}\nRaw response data: ${raw}`);
                throwError(`Pairs info not found for GET request to market '${Market.Binance}'`);
            }
            else {
                resolve(marketData);
            }
        }) as PairObject[];
        resolve(data);
    });
}

function callPoloniex(pool: Pool): Promise<void> {
    return new Promise(resolve => {
        axios.get('https://poloniex.com/public?command=returnTicker')
            .then(async function (response: AxiosResponse) {
                let data: PairObject[] = await prepareData(response.data as PoloniexResponse) as PairObject[];
                data = await renamePairs(data) as PairObject[];
                await writeToDb(pool, data);
                resolve();
            })
            .catch((error: Error) => {
                console.error(`Poloniex is unreachable or something else gone wrong. ${error}`);
                resolve();
            });
    })
}

export { callPoloniex };