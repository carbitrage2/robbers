function delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
}

async function throwError(message: string): Promise<void> {
    return new Promise(() => {
        throw new Error(message);
    });
}

export { delay, throwError };