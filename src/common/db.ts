import { Pool, PoolClient } from 'pg';
import 'dotenv';

async function createPool(): Promise<Pool> {
    return new Promise<Pool>(resolve => {
        const pool: Pool = new Pool(
            {
                user: process.env.DB_USER, 
                host: process.env.DB_HOST, 
                port: +process.env.DB_PORT!, 
                database: process.env.DB_NAME, 
                password: process.env.DB_PASSWORD
            }
        );
        resolve(pool);
    });
}

async function sendData(pool: Pool, data: PairObject[]): Promise<void> {
    await new Promise<void>(async resolve => {
        pool.connect(async (error: Error, client: PoolClient, release: any) => {
            for await (let value of data) {
                await client
                    .query(
                        `UPDATE pairs_and_markets SET price = $1, date = $2 WHERE pair = $3 and market = $4;`,
                        [value.price, value.date, value.pair, value.market])
                if (error) {
                    console.error(`Database ${error.stack}`);
                }
            }
            release();
            resolve();
        });
    });
}

async function writeToDb(pool: Pool, data: PairObject[]): Promise<void> {
    return new Promise<void>(async resolve => {
        await sendData(pool, data);
        resolve();
    });
}

export { createPool, writeToDb };